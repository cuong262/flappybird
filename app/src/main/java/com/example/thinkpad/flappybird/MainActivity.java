package com.example.thinkpad.flappybird;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    List<Contact> list=new ArrayList<>();
    adapter adapter1;
    Button btnAdd;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Example of a call to a native method
        listView=(ListView)findViewById(R.id.listview);
        Contact c=new Contact(1,"cuong","01627102063",1);
        Contact c1=new Contact(1,"cuong","01627102063",1);
        Contact c2=new Contact(1,"cuong","01627102063",1);
        list.add(c);
        list.add(c1);
        list.add(c2);
        adapter1 =new adapter(list,getBaseContext());
        listView.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();
        btnAdd=findViewById(R.id.Add);
        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                NewContact n=new NewContact();
                n.onCreate(savedInstanceState);
            }
        });
    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
