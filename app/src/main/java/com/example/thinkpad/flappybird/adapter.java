package com.example.thinkpad.flappybird;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by THINKPAD on 01-Feb-18.
 */

public class adapter extends BaseAdapter {
    private List<Contact> list=new ArrayList<>();
    private Context context;

    public adapter(List<Contact> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Cuong cuong;
        if(view==null){
            view=View.inflate(context,R.layout.item,null);
            cuong = new Cuong();
            cuong.name=(TextView)view.findViewById(R.id.textView);
            cuong.phone=(TextView)view.findViewById(R.id.textView2);
            cuong.group=(TextView)view.findViewById(R.id.textView3);
            cuong.img=(ImageView)view.findViewById(R.id.imageView2);
            view.setTag(cuong);
        }else{
            cuong=(Cuong)view.getTag();
        }
        Contact contact=(Contact) getItem(i);
        if(contact!=null){
            cuong.name.setText(contact.getName());
            cuong.phone.setText(contact.getPhone());
            cuong.group.setText(String.valueOf(contact.getGroup()));
            cuong.img.setImageResource(R.drawable.big_bean);
        }

        return view;
    }
    public static class Cuong{
        TextView name;
        TextView  phone;
        TextView group;
        ImageView img;

    }
}
