package com.example.thinkpad.flappybird;

import java.util.ArrayList;

/**
 * Created by THINKPAD on 01-Feb-18.
 */

public class ListContact {

    ArrayList<Contact> listContact=new ArrayList<>();

    public ListContact() {
        this.makeList();
    }

    public ListContact(ArrayList<Contact> listContact) {
        this.listContact = listContact;
    }
    private void makeList(){
        for(int i=0;i<10;i++){
            Contact c=new Contact();
            c.setId(i);
            c.setName("Cuong");
            c.setPhone("01627102063");
            c.setGroup(i);
            listContact.add(c);
        }
    }
    public ArrayList<Contact> getListContact() {
        return listContact;
    }

    public void setListContact(ArrayList<Contact> listContact) {
        this.listContact = listContact;
    }
}
