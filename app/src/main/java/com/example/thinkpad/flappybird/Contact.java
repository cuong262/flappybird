package com.example.thinkpad.flappybird;

/**
 * Created by THINKPAD on 01-Feb-18.
 */

public class Contact {
    int id;
    String name;
    String phone;
    int group;

    public Contact(int id, String name, String phone, int group) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.group = group;
    }

    public Contact() {
        this.id=0;
        this.name=null;
        this.phone=null;
        this.group=0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getGroup() {
        return group;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGroup(int group) {
        this.group = group;
    }
}
